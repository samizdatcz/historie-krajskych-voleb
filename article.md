---
title: "Doleva, či doprava. Podívejte se, jak volí vaše obec"
perex: "Český rozhlas připravil aplikaci, kde zjistíte, čím je vaše obec v kraji výjimečná: Které strany zde vedou, a které naopak propadají."
description: "Velký přehled historických výsledků krajských voleb v obcích. Podívejte se, čím se vaše obec liší od zbytku kraje."
authors: ["Jan Cibulka", "Marcel Šulek", "Dominika Píhová"]
published: "12. září 2016"
socialimg: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/media/socimg.jpg
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "historie-krajskych-voleb"
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
recommended:
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější.
    image: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/kandidati-do-senatu/
    title: Český rozhlas představuje kandidáty do Senátu
    perex: Šest otázek a stejný časový limit. Český rozhlas představuje všechny kandidáty do Senátu, kteří se rozhodli využít nabídku na natočení takzvaných předvolebních vizitek.
    image: https://interaktivni.rozhlas.cz/kandidati-do-senatu/media/mapa.png
  - link: https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/
    title: Český rozhlas představuje kandidáty do krajských zastupitelstev
    perex: Regionální stanice Českého rozhlasu nabídly všem jedničkám na kandidátkách stejný prostor a možnost odpovědět na stejné otázy; celkem oslovily 269 kandidátů.
    image: https://interaktivni.rozhlas.cz/lidri-krajskych-kandidatek/media/mapa.png
---

Český rozhlas zpracoval výsledky krajských voleb od roku 2000, můžete si tak prohlédnout, zda vaši sousedé dlouhodobě podporují některé partaje, nebo volí každé čtyři roky jinak. Stačí vepsat jméno obce do horního okénka aplikace, výsledky se automaticky přepíší.

Jako příklad poslouží Nová Bystřice na Jindřichohradecku. Pokud totiž vezmeme v potaz obce, kde se v posledních krajských volbách sešlo alespoň tisíc platných hlasů, tak právě tato je baštou sociální demokracie. Ta zde před čtyřmi lety sesbírala téměř 52 % hlasů.

V následující aplikaci zde má ČSSD index +2,93. To znamená, že se místní výsledky vychylují ve prospěch sociálních demokratů třikrát více, než je tomu průměrně ve zbytku Jihočeského kraje. Konkrétní výsledky strany se pak zobrazí po najetí myší.

<aside class="big">
  <figure>
    <iframe src="https://interaktivni.rozhlas.cz/data/kraj-volby-history/www/_index.html#546798" height="410"></iframe>
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/kz2012/kz?xjazyk=CZ&xdatum=20121012" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

Naopak zde spíše propadají zelení, KDU-ČSL nebo lokální strany, jako Doktoři (za uzdravení společnosti) nebo Jihočeši 2012.

Z mapy celkových výsledků ČSSD v minulých krajských volbách je pak vidět, že na Jidřichohradecku a v západní části Vysočiny mají sociální demokraté obecně silnou podporu. *"ČSSD má na Vysočině tradičně velmi solidní volební výsledky, takže to není nic překvapivého. Vysočina je kraj, ve kterém se jí dlouhodobě daří,"* říká k tomu politolog Tomáš Lebeda z Univerzity Palackého v Olomouci. 

<aside class="big">
  <figure>
    <img src="./media/b_cssd.jpg">
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/kz2012/kz?xjazyk=CZ&xdatum=20121012" target="_blank">Volby.cz</a>
  </figcaption>
</aside> 

Na následující mapě jsou vidět naopak výsledky ODS, strana byla v roce 2012 nejsilnější v Plzeňském kraji a na severu a severozápadě Středočeského kraje. Právě na Domažlicku se nachází obec Kdyně, kde občanští demokraté získali 41 %. Politolog Lebeda za úspěchem ODS na Plzeňsku vidí tehdejšího lídra Jiřího Pospíšila: *"Byl v té době velmi populárním politikem ODS a zcela pochopitelně dokázal přitáhnout velké množství voličů. Dokonce byl o rok později lídrem za ODS v Plzeňském kraji ve sněmovních volbách."*

<aside class="big">
  <figure>
    <img src="./media/b_ods.jpg">
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/kz2012/kz?xjazyk=CZ&xdatum=20121012" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

Že je tady tato strana tradičně silná, ukazuje i aplikace s historickými čísly, ODS zde vede s indexem 2,84. 

<aside class="big">
  <figure>
    <iframe src="https://interaktivni.rozhlas.cz/data/kraj-volby-history/www/_index.html#553786" height="410"></iframe>
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/kz2012/kz?xjazyk=CZ&xdatum=20121012" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

Na opačném konci pomyslného politického spektra se nachází Albrechtice u Bruntálu v Moravskoslezském kraji, skoro 45 % hlasů zde v roce 2012 utržili komunisti, podobně si vedli v celé severozápadní části kraje. 

<aside class="big">
  <figure>
    <img src="./media/b_kscm.jpg">
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/kz2012/kz?xjazyk=CZ&xdatum=20121012" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

V Albrechticích historicky sbírala hlasy i ČSNS, Strana práv občanů (dříve Zemanovci) a třeba i Republikáni Miroslava Sládka. v roce 2004 tady pak krátce zazářilo také SNK sdružení nezávislých. Naopak TOP 09 se Starosty jde jasně propadla.

<aside class="big">
  <figure>
    <iframe src="https://interaktivni.rozhlas.cz/data/kraj-volby-history/www/_index.html#597635" height="410"></iframe>
  </figure>
  <figcaption>
    Zdroj: <a href="http://volby.cz/pls/kz2012/kz?xjazyk=CZ&xdatum=20121012" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

Komunistické straně se dařilo i v Ústeckém a Karlovarském kraji, zčásti na úkor ČSSD. Podle Lebedy tady hrála svou roli i voličská účast, která je v krajských volbách nižší než třeba v těch sněmovních. *"Nízká účast právě výrazně pomáhá volebním ziskům komunistické strany. Úplně jednoduše řečeno – sociálnědemokratický volič častěji zůstane sedět doma,"* vysvětluje.